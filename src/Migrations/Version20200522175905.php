<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200522175905 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE meals DROP FOREIGN KEY FK_E229E6EA7B478B1A');
        $this->addSql('DROP INDEX IDX_E229E6EA7B478B1A ON meals');
        $this->addSql('ALTER TABLE meals CHANGE categories_id_id categories_id INT NOT NULL');
        $this->addSql('ALTER TABLE meals ADD CONSTRAINT FK_E229E6EAA21214B7 FOREIGN KEY (categories_id) REFERENCES categories (id)');
        $this->addSql('CREATE INDEX IDX_E229E6EAA21214B7 ON meals (categories_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE meals DROP FOREIGN KEY FK_E229E6EAA21214B7');
        $this->addSql('DROP INDEX IDX_E229E6EAA21214B7 ON meals');
        $this->addSql('ALTER TABLE meals CHANGE categories_id categories_id_id INT NOT NULL');
        $this->addSql('ALTER TABLE meals ADD CONSTRAINT FK_E229E6EA7B478B1A FOREIGN KEY (categories_id_id) REFERENCES categories (id)');
        $this->addSql('CREATE INDEX IDX_E229E6EA7B478B1A ON meals (categories_id_id)');
    }
}
