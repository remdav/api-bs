<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     collectionOperations={"POST"},
 *     itemOperations={"GET"},
 *     normalizationContext={"groups" = {"read:user"}},
 *     denormalizationContext={"disable_type_enforcement" = true}
 * )
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="users", indexes={
 *     @Index(name="firstname", columns={"firstname"}),
 *     @Index(name="name", columns={"name"}),
 *     @Index(name="username", columns={"username"})
 * })
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"read:user"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups({"read:user"})
     * @Assert\NotBlank()
     * @Assert\Length(min="2", max="180")
     */
    private $username;

    /**
     * @ORM\Column(type="json")
     * @Groups({"read:user"})
     * @Assert\NotBlank()
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @Assert\Length(min="2", max="255")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"read:user"})
     * @Assert\NotBlank()
     * @Assert\Length(min="2", max="100")
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"read:user"})
     * @Assert\NotBlank()
     * @Assert\Length(min="2", max="100")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=150)
     * @Groups({"read:user"})
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $addressMail;

    /**
     * @ORM\Column(type="string", length=10)
     * @Groups({"read:user"})
     * @Assert\NotBlank()
     * @Assert\Length(min="10", max="10")
     * @Assert\Type("numeric")
     */
    private $phoneNumber;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read:user"})
     * @Assert\NotBlank()
     * @Assert\Length(min="2", max="100")
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=5)
     * @Groups({"read:user"})
     * @Assert\NotBlank()
     * @Assert\Length(min="5", max="5")
     * @Assert\Type("numeric")
     */
    private $zipCode;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAddressMail(): ?string
    {
        return $this->addressMail;
    }

    public function setAddressMail(string $addressMail): self
    {
        $this->addressMail = $addressMail;

        return $this;
    }

    public function getPhoneNumber(): ?int
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getZipCode(): ?int
    {
        return $this->zipCode;
    }

    public function setZipCode(string $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }
}
