<?php

namespace App\Events\Jwt;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\ExternalUser;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Gesdinet\JWTRefreshTokenBundle\Model\RefreshTokenManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Serializer\SerializerInterface;

class ConnectionJwtSubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $jwtManager;
    /**
     * @var RefreshTokenManagerInterface
     */
    private $refreshToken;
    /**
     * @var RefreshTokenManagerInterface
     */
    private $em;

    public function __construct(JWTTokenManagerInterface $jwtManager, RefreshTokenManagerInterface $refreshToken, EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->refreshToken = $refreshToken;
        $this->jwtManager = $jwtManager;
    }

    public static function getSubscribedEvents()
    {
        return [KernelEvents::VIEW => ["GenerateTokenLogin", EventPriorities::PRE_SERIALIZE]];
    }

    public function generateJwt($user)
    {
        $refreshToken = $this->refreshToken->create();
        $refreshToken->setRefreshToken();
        $refreshToken = $refreshToken->getRefreshToken();
        $response = new JsonResponse(['token' => $this->jwtManager->create($user), "refreshToken" => $refreshToken ]);
        return $response->send();
    }

    public function GenerateTokenLogin(ViewEvent $event)
    {
        $user = $event->getControllerResult();

        if(($user instanceof User || $user instanceof ExternalUser) && $event->getRequest()->getMethod() === "POST")
        {
            $this->generateJwt($user);
            die();
        }
    }
}