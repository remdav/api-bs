<?php


namespace App\Events\Jwt;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;

class JwtCreatedListener
{
    public function onJWTCreated(JWTCreatedEvent $event) {
        $user = $event->getUser();
        $fullName = "{$user->getFirstName()} {$user->getName()}";
        $payload = $event->getData();

        $payload["name"] = $fullName;
        $event->setData($payload);
    }
}