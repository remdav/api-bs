<?php

namespace App\Events\User;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\ExternalUser;
use App\Entity\User;
use App\Events\Jwt\ConnectionJwtSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class AddRoleUserRegistrationSubscriber implements EventSubscriberInterface
{
    private $connected;
    private $em;

    public function __construct(ConnectionJwtSubscriber $connected, EntityManagerInterface $em)
    {
        $this->connected = $connected;
        $this->em = $em;
    }

    public static function getSubscribedEvents()
    {
        return [KernelEvents::VIEW => ["addRoleUser", EventPriorities::PRE_VALIDATE]];
    }


    public function addRoleUser(ViewEvent $event)
    {
        $user = $event->getControllerResult();

        if(($user instanceof User || $user instanceof ExternalUser) && $event->getRequest()->getMethod() === "POST")
        {
            if($user instanceof ExternalUser)
            {
                $userExist = $this->em->getRepository(ExternalUser::class)->findOneBy(['idExternal' => $user->getIdExternal()]);

                if($userExist)
                {
                    $this->connected->generateJwt($userExist);
                    die();
                }
            }

            $user->setRoles(["ROLE_USER"]);
        }
    }
}