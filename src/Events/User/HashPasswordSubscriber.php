<?php


namespace App\Events\User;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class HashPasswordSubscriber implements EventSubscriberInterface
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encode;

    public function __construct(UserPasswordEncoderInterface $encode)
    {
        $this->encode = $encode;
    }

    public static function getSubscribedEvents()
    {
        return [KernelEvents::VIEW => ['passwordEncode', EventPriorities::PRE_WRITE]];
    }

    public function passwordEncode(ViewEvent $event)
    {
        $user = $event->getControllerResult();

        if($user instanceof User && $event->getRequest()->getMethod() === "POST")
        {
            $user->setPassword($this->encode->encodePassword($user, $user->getPassword()));
        }
    }
}